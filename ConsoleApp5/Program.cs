﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {


        static void Main(string[] args)
        {
            // lisanduud rida
               
            switch (DateTime.Now.DayOfWeek)
            {

                case DayOfWeek.Sunday:
                    Console.WriteLine("täna saab õlut juua");
                    break;

                case DayOfWeek.Saturday:
                    Console.WriteLine("täna saab sauna");
                    goto case DayOfWeek.Sunday;

                case DayOfWeek.Wednesday:
                    Console.WriteLine("täna saab sulgpalli trenni minna");
                    break;

                default:
                    Console.WriteLine("täna ei toimu midgai huvitavat");
                    break;
            }

            if (DateTime.Now.DayOfWeek == DayOfWeek.Sunday || DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
            {
                
                if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
                {
                    // ainult laupäeva asjad - saun
                }
                // nädalavahetuse asjad - õlut
            }
            else if (DateTime.Now.DayOfWeek == DayOfWeek.Wednesday)
            {
                // kolmapäevased asjad - trenn
            }
            else
            {
                // muud asjad - igav
            }

            // Massiivid e array

            int[] arvud = { 1, 2, 3, 4, 7, 9, 2 };

            // tsükkel

            for (int i = 0; i < arvud.Length; i++)
            {
                // need kaks rida on sisult samaväärsed
                Console.WriteLine("arv number {0} on {1}", i, arvud[i]);
                Console.WriteLine($"arv number {i} on {arvud[i]}" );
            }

            foreach (int x in arvud)
            {
                Console.WriteLine(x);
            }


            int[][] massiivideMassiiv = new int[][]
            {
                new int[] { 1, 2, 3 },
                new int[] { 1, 2 },
                new int[]{ 1, 2, 3, 4 }
            };
            //misasiSeeOn[0] = new int[2] { 1, 2 };
            //misasiSeeOn[1] = new int[4] { 1, 2, 3, 4 };
            //misasiSeeOn[2] = new int[1] { 7 };
            massiivideMassiiv[0][2] = 99;

            // arvutame misasiseeon summa
            int misSumma = 0;
            int loendur = 0;
            foreach (int[] rida in massiivideMassiiv)
            {
                loendur += rida.Length; // kas arvutan siin
                foreach (int x in rida)
                {
                    misSumma += x;
                    //loendur++;  // või arvutan siin (aga mitte mõlemas)
                }
            }


            

            Console.WriteLine(misSumma);
            Console.WriteLine(misSumma / loendur);
            Console.WriteLine("trükime massiivide massiivi");
            foreach (int[] x in massiivideMassiiv)
            {
                foreach (int y in x)
                    Console.Write($"{y} ");
                Console.WriteLine();
            }


            int[,] tabel = { { 1, 2, 3, 0 }, { 4, 5, 6, 7 }, { 6, 8, 9, 10 } };
            tabel[0, 3] = 99;
            
            //foreach (int x in tabel) Console.WriteLine(x);

            for (int i = 0; i < tabel.GetLength(0); i++)
            {
                for (int j = 0; j < tabel.GetLength(1); j++)
                {
                    Console.Write($"{tabel[i,j]} ");
                }
                Console.WriteLine();
            }

            int summa = 0;
            foreach (int x in tabel) summa += x;
            Console.WriteLine($"summa on {summa}");



            summa = 0;
            for (int i = 0; i < arvud.Length; i++)
            {
                summa += arvud[i];
            }
            Console.WriteLine($"teine summa on {summa}");

            // leiame arvudest kõige väiksema

            int väikseim = arvud[0];
            foreach (int x in arvud) if (x < väikseim) väikseim = x;
                
            
            // leia, mitmes arvudest on käige väiksem

            int iväike = 0;
            for (int i = 1; i < arvud.Length; i++)
                if (arvud[i] < arvud[iväike]) iväike = i;




            // while tsükli näiteid koos for ja foreachiga
            Console.WriteLine("esimene variant - while");
            {
                int n = 0; // see on 1. avaldis FOR tsüklist
                while (n < arvud.Length) // see on 2. avaldis FOR tsüklist
                {
                    Console.WriteLine(arvud[n]);
                    n++;// see on 3. avaldis FOR tsüklist
                }
                 
            }
            // on sama, mis
            Console.WriteLine("teine variant - for");

            for (int n = 0; n < arvud.Length; n++)
            {
                Console.WriteLine(arvud[n]);
            }

            // foreach on pisut keerukam
            Console.WriteLine("kolmas variant - while");

            {
                var _ = arvud.GetEnumerator();
                while (_.MoveNext())
                {
                    int x = (int)_.Current;
                    Console.WriteLine(x);
                }
            }

            // on sama, mis
            Console.WriteLine("neljas variant - foreach");
            foreach (int x in arvud)
            {
                Console.WriteLine(x);
            }


        }
    }
}
